{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "5266d507",
   "metadata": {},
   "source": [
    "# Harmonic Bases -- a Python Implementation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "54552271-748c-4f7e-9cf0-17d3e40024ba",
   "metadata": {},
   "source": [
    "This notebook demonstrates the calculation of harmonic basis tensors as numpy arrays. Symbolic tensor calculations are supported by using numpy arrays containing sympy expressions. As harmonic bases can be defined for arbitrary order, most parts of this code simply define order-dependent functions to support arbitrary-order needs. No part of this code is particularly optimized. For most applications, the application-specific harmonic basis can simply be hardcoded once computed using this code.\n",
    "\n",
    "This jupyter notebook is supplementary to the paper \"Tensorial harmonic bases of arbitrary order with applications in elasticity, elastoviscoplasticity and texture-based modeling\" (Krause and Böhlke, 2024, to be published).\n",
    "\n",
    "The text and code contained in this notebook is licensed under a Creative Commons CC BY-SA 4.0 license. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ed5ea511-2a04-4112-917a-240ba306a7f1",
   "metadata": {},
   "source": [
    "## Preliminaries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "35411e42",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import numpy.typing as npt\n",
    "import sympy\n",
    "from sympy.physics.quantum.spin import Rotation as sympy_rot\n",
    "import string\n",
    "np.set_printoptions(linewidth=1000)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "e6da8a89",
   "metadata": {},
   "outputs": [],
   "source": [
    "def adjungate(arr: npt.NDArray):\n",
    "    \"\"\"Adjungate of complex numpy array.\"\"\"\n",
    "    return np.conjugate(arr.T)\n",
    "\n",
    "np_simplify = np.vectorize(sympy.simplify)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b085abd1-1ea3-4a90-a2dd-84f0659ed089",
   "metadata": {},
   "source": [
    "For symbolic computation, this code uses numpy object arrays containing sympy expressions.\n",
    "The default implementation of numpy's einsum doesn't work with object arrays.\n",
    "Therefore, this code uses oinsum from https://gist.github.com/jcmgray/906ba067f0b4ab4a1a1adde06903b213,\n",
    "which is reproduced here with permission (and not covered by this notebook's license)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "2e28bbdc",
   "metadata": {},
   "outputs": [],
   "source": [
    "def oinsum(eq, *arrays):\n",
    "    \"\"\"A ``einsum`` implementation for ``numpy`` object arrays.\n",
    "    \"\"\"\n",
    "    import functools\n",
    "    import operator\n",
    "\n",
    "    lhs, output = eq.split('->')\n",
    "    inputs = lhs.split(',')\n",
    "\n",
    "    sizes = {}\n",
    "    for term, array in zip(inputs, arrays):\n",
    "        for k, d in zip(term, array.shape):\n",
    "            sizes[k] = d\n",
    "\n",
    "    out_size = tuple(sizes[k] for k in output)\n",
    "    out = np.empty(out_size, dtype=object)\n",
    "\n",
    "    inner = [k for k in sizes if k not in output]\n",
    "    inner_size = [sizes[k] for k in inner]\n",
    "\n",
    "    for coo_o in np.ndindex(*out_size):\n",
    "\n",
    "        coord = dict(zip(output, coo_o))\n",
    "\n",
    "        def gen_inner_sum():\n",
    "            for coo_i in np.ndindex(*inner_size):\n",
    "                coord.update(dict(zip(inner, coo_i)))\n",
    "\n",
    "                locs = [tuple(coord[k] for k in term) for term in inputs]\n",
    "                elements = (array[loc] for array, loc in zip(arrays, locs))\n",
    "                yield functools.reduce(operator.mul, elements)\n",
    "\n",
    "        out[coo_o] = functools.reduce(operator.add, gen_inner_sum())\n",
    "\n",
    "    return out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c50a67c2-2010-463a-ae75-027dcc56bb1c",
   "metadata": {},
   "source": [
    "## SO(3) and so(3)\n",
    "To represent the action of the group SO(3) on tensors of arbitrary order, i.e. rotations, we use the Rayleigh product ( $\\star$ ). The infinitesimal rayleigh product ( $\\boxtimes$ ) similarly represents the action of the Lie algebra so(3) on a tensor. Both products are available as exponential variants, such that $A \\star B = A^{\\star n} [B]$ for an n-th order tensor $B$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "9c1f840d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Action of SO(3) and so(3) on tensor spaces.\n",
    "def rayleigh_power(t: npt.NDArray, n: int):\n",
    "    \"\"\"Rayleigh (or Kronecker) power.\"\"\"\n",
    "    assert len(t.shape) == 2\n",
    "    indices = string.ascii_lowercase\n",
    "    input_str = \", \".join([indices[i]+indices[i+n] for i in range(n)])\n",
    "    output_str = indices[:2*n]\n",
    "    return oinsum(input_str+\"->\"+output_str, *[t]*n)\n",
    "\n",
    "\n",
    "def inf_rayleigh(t: npt.NDArray, b: npt.NDArray):\n",
    "    \"\"\"Infinitesimal rayleigh product.\"\"\"\n",
    "    assert len(t.shape) == 2\n",
    "    b_order = len(b.shape)\n",
    "    indices = string.ascii_lowercase\n",
    "    base_string = indices[:b_order]\n",
    "    t_strings = [base_string[i]+\"z\" for i in range(b_order)]\n",
    "    b_strings = [base_string[:i]+\"z\"+base_string[i+1:] for i in range(b_order)]\n",
    "    result = np.zeros(b.shape, dtype=object)\n",
    "    for t_s, b_s in zip(t_strings, b_strings):\n",
    "        result += oinsum(t_s+\", \"+b_s+\"->\"+base_string, t, b)\n",
    "    return result\n",
    "\n",
    "\n",
    "def inf_rayleigh_power(t: npt.NDArray, n: int):\n",
    "    \"\"\"Infinitesimal rayleigh power of t.\"\"\"\n",
    "    assert len(t.shape) == 2\n",
    "    if n == 1:\n",
    "        return t\n",
    "    indices = string.ascii_lowercase\n",
    "    einsum_str = (\",\".join([f\"{indices[i]}{indices[i+n]}\" for i in range(n)])\n",
    "                  + f\"->{indices[:2*n]}\")\n",
    "    eye = np.eye(3, dtype=int)\n",
    "    arg = [[eye]*i + [t] + [eye]*(n-i-1) for i in range(n)]\n",
    "    summands = [oinsum(einsum_str, *a) for a in arg]\n",
    "    result = np.zeros(summands[0].shape, dtype=object)\n",
    "    for s in summands:\n",
    "        result += s\n",
    "    return result"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b339c3d1-adab-4b4f-859d-c0e7ae71e942",
   "metadata": {},
   "source": [
    "We give a representation of the action of so(3) on first-order tensors via the second-order tensors $J_i$, here called `J_1` to distinguish them from the action on higher-order tensors. These are also called angular momentum operators in quantum mechanics. The higher-order angular momentum operators are provided via a function `J`. On the basis of these operators, we define the ladder operators, which can be used to calculate eigenvectors of so(3)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "118ebe09",
   "metadata": {},
   "outputs": [],
   "source": [
    "# so(3) operators.\n",
    "levi_civita = np.zeros((3, 3, 3), dtype=object)\n",
    "for i in range(3):\n",
    "    for j in range(3):\n",
    "        if j != i:\n",
    "            k = 3-(j+i)\n",
    "            if 1 in (i, j):\n",
    "                levi_civita[i, j, k] = sympy.Integer(j-i)\n",
    "            else:\n",
    "                levi_civita[i, j, k] = sympy.Integer((i-j)/2)\n",
    "\n",
    "\n",
    "e = [np.eye(3, dtype=int)[i] for i in range(3)]\n",
    "\n",
    "J_1 = [sympy.I*levi_civita@(e[i]) for i in range(3)]\n",
    "\n",
    "\n",
    "def J(n: int):\n",
    "    \"\"\"n-th order momentum operators.\"\"\"\n",
    "    return [inf_rayleigh_power(J_1[i], n) for i in range(3)]\n",
    "\n",
    "\n",
    "def ladder(n: int):\n",
    "    \"\"\"n-th order ladder operators (z convention).\"\"\"\n",
    "    Js = J(n)\n",
    "    return Js[0] - sympy.I*Js[1], Js[0] + sympy.I*Js[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39835614-9faa-4ed8-adfc-02d7c39d84aa",
   "metadata": {},
   "source": [
    "The following code allows for the calculation of eigentensors of so(3)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "9e853cfa",
   "metadata": {},
   "outputs": [],
   "source": [
    "@staticmethod\n",
    "def v_min(n: int):\n",
    "    \"\"\"Calculate minimal eigentensor for z-axis rotation.\"\"\"\n",
    "    assert n > 0, n\n",
    "    assert int(n) == n, n\n",
    "    if n == 1:\n",
    "        return (e[0] + sympy.I*e[1])/sympy.sqrt(2)\n",
    "    return np.tensordot(v_min(n-1), v_min(1), axes=0)\n",
    "\n",
    "@staticmethod\n",
    "def v_dict(n: int):\n",
    "    \"\"\"n-th order so(3) eigentensor from v_min.\"\"\"\n",
    "    v = {}\n",
    "    v[-n] = v_min(n)\n",
    "    J_plus, J_minus = ladder(n)\n",
    "    for k in range(-n, n):\n",
    "        fac = sympy.sqrt((n-k)*(n+k+1))\n",
    "        v[k+1] = np.tensordot(J_plus, v[k]/fac, axes=n)\n",
    "    return v"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a5080a50-aacb-41e6-a2b5-d4b2e88ae367",
   "metadata": {},
   "source": [
    "## Deviatoric Basis Tensors\n",
    "\n",
    "The relationship between the complex eigenvector bases and the fully real deviatoric bases is defined via a transformation matrix $T$, which we implement as an order-dependent function. Using $T$, the deviatoric eigentensors are defined. As $T$ is sparse, this is by no means an efficient implementation, but we hope it is at least clear to understand."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "6d46ff1a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Deviatoric bases defined via transformation matrices.\n",
    "def T_dv(n: int):\n",
    "    \"\"\"n-th order eigenvector-to-dev transformation.\"\"\"\n",
    "    T_dv = np.zeros((2*n+1, 2*n+1), dtype=object)\n",
    "    T_dv[2*n, n] = 1\n",
    "    for m in range(1, n+1):\n",
    "        T_dv[2*(n-m), n-m] = 1/sympy.sqrt(2)\n",
    "        T_dv[2*(n-m), n+m] = (-1)**m/sympy.sqrt(2)\n",
    "        T_dv[2*(n-m)+1, n-m] = -sympy.I/sympy.sqrt(2)\n",
    "        T_dv[2*(n-m)+1, n+m] = -sympy.I*(-1)**(m+1)/sympy.sqrt(2)\n",
    "    return T_dv\n",
    "\n",
    "\n",
    "def D(n: int):\n",
    "    \"\"\"n-th order deviatoric basis tensors.\"\"\"\n",
    "    if n == 0:\n",
    "        return [1]\n",
    "    vs = v_dict(n)\n",
    "    result = []\n",
    "    T_dv_n = T_dv(n)\n",
    "    for i in reversed(range(1, n+1)):\n",
    "        # note: the results are fully real regardless of np.real,\n",
    "        # this just casts complex to float.\n",
    "        result.append((T_dv_n[len(result), n+i]*vs[i]\n",
    "                           + T_dv_n[len(result), n-i]*vs[-i]))\n",
    "        result.append((T_dv_n[len(result), n+i]*vs[i]\n",
    "                           + T_dv_n[len(result), n-i]*vs[-i]))\n",
    "    result.append((vs[0]))\n",
    "    return [np_simplify(a) for a in result]\n",
    "\n",
    "\n",
    "def T_de(n: int):\n",
    "    \"\"\"n-th order e^n-to-dev transformation.\"\"\"\n",
    "    T_de = np.zeros((2*n+1,)+(3,)*n)\n",
    "    Dn = D(n)\n",
    "    for j in range(len(Dn)):\n",
    "        T_de[j, ...] = Dn[j].array\n",
    "    return T_de"
   ]
  },
  {
   "cell_type": "raw",
   "id": "836cba32-ec51-4e5b-b46b-00d02263b3e5",
   "metadata": {},
   "source": [
    "## Harmonic basis tensors\n",
    "\n",
    "For the zeroth- and first-order tensor spaces, which are each fully deviatoric, the harmonic basis tensors are simply the deviatoric basis tensors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "21546a2f-8064-45e1-a2fb-b6e5647d5a54",
   "metadata": {},
   "outputs": [],
   "source": [
    "r0_harm = D(0)\n",
    "r1_harm = D(1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "6a4a64b0-1a7e-4e3a-8b3f-b2eedede627f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Zeroth-order harmonic basis\n",
      "[1]\n",
      "First-order harmonic basis\n",
      "[array([1, 0, 0], dtype=object), array([0, 1, 0], dtype=object), array([0, 0, 1], dtype=object)]\n"
     ]
    }
   ],
   "source": [
    "print(\"Zeroth-order harmonic basis\")\n",
    "print(r0_harm)\n",
    "print(\"First-order harmonic basis\")\n",
    "print(r1_harm)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28d56b90-ec6b-401c-bcd7-fc07cec7c320",
   "metadata": {},
   "source": [
    "Harmonic basis tensors of higher order are calculated from deviatoric basis tensors using the inclusion tensors. The inclusion tensors rely on Clebsch-Gordan tensors $c^{nmo}$. We use the implementation of their coefficients in the complex eigenvector basis (`cg_v`) given by sympy. The coefficients in the deviatoric basis (`cg_D`) can be calculated via $T$. Finally, the coefficients of the Clebsch-Gordan tensors in the underlying $\\mathbb{R}^3$ basis $e_i$ are given by `cg`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "439838c3-ee41-4690-bcbc-0774868be669",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Clebsch-Gordan tensors.\n",
    "def cg_v(n, m, o):\n",
    "    \"\"\"Coefficients of CG tensors in the eigenvector basis.\"\"\"\n",
    "    result = np.zeros((2*n+1, 2*m+1, 2*o+1), dtype=object)\n",
    "    for i in range(-n, n+1):\n",
    "        for j in range(-m, m+1):\n",
    "            for k in range(-o, o+1):\n",
    "                cg = sympy.physics.quantum.cg.CG(n, i, m, j, o, k).doit()\n",
    "                result[i+n, j+m, k+o] = cg\n",
    "    return result\n",
    "\n",
    "\n",
    "def cg_D(n, m, o):\n",
    "    \"\"\"Coefficients of CG tensors in the D basis.\"\"\"\n",
    "    cg = cg_v(n, m, o)\n",
    "    T_dv_n = T_dv(n)\n",
    "    T_dv_m = T_dv(m)\n",
    "    T_dv_o = T_dv(o)\n",
    "    \n",
    "    result = oinsum(\"ai,bj,cl,ijl->abc\", T_dv_n, T_dv_m, np.conjugate(T_dv_o), cg)*sympy.I**(n+m+o)\n",
    "    return np.real(result)\n",
    "\n",
    "\n",
    "def cg(n, m, o):\n",
    "    \"\"\"CG tensors in the e_i basis.\"\"\"\n",
    "    D_n = D(n)\n",
    "    D_m = D(m)\n",
    "    D_o = D(o)\n",
    "    cg_D_nmo = cg_D(n, m, o)\n",
    "    if n+m+o > 0:\n",
    "        result = np.zeros((3,)*(n+m+o), dtype=object)\n",
    "    else:\n",
    "        result = 0\n",
    "    for i1, D_ni in enumerate(D_n):\n",
    "        for i2, D_mi in enumerate(D_m):\n",
    "            for i3, D_oi in enumerate(D_o):\n",
    "                tens = np.tensordot(np.tensordot(D_ni, D_mi, axes=0),\n",
    "                                    D_oi, axes=0)\n",
    "                result += cg_D_nmo[i1, i2, i3]*tens\n",
    "    return result"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2db271d6-72d7-4497-ad55-16e3f27a5098",
   "metadata": {},
   "source": [
    "For illustrative purposes, we calculate the inclusion tensors for the space of second-order symmetric tensors. These are simply Clebsch-Gordan tensors. Subsequently, we define a second-order harmonic basis using these inclusion tensors. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "c90d49a9",
   "metadata": {},
   "outputs": [],
   "source": [
    "def P2_sym(k):\n",
    "    \"\"\"Inclusion tensors for Sym based for a given deviatoric subspace order k.\"\"\"\n",
    "    if k == 0:\n",
    "        return cg(1, 1, 0)\n",
    "    elif k == 2:\n",
    "        return cg(1, 1, 2)\n",
    "    else:\n",
    "        raise ValueError(\"The symmetric second order tensors have no deviatoric subspace of order k.\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "5db7d1a6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Full and symmetric harmonic bases of second order tensor spaces.\n",
    "r2_harm = [np.tensordot(cg(1,1,k), D(k)[i], axes=k) for k in range(3) for i in range(2*k+1)]\n",
    "sym_harm = [np.tensordot(P2_sym(k), D(k)[i], axes=k) for k in [0,2] for i in range(2*k+1)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "2d8a937d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Second-order (full) harmonic basis\n",
      "[[sqrt(3)/3 0 0]\n",
      " [0 sqrt(3)/3 0]\n",
      " [0 0 sqrt(3)/3]]\n",
      "[[0 0 0]\n",
      " [0 0 sqrt(2)/2]\n",
      " [0 -sqrt(2)/2 0]]\n",
      "[[0 0 -sqrt(2)/2]\n",
      " [0 0 0]\n",
      " [sqrt(2)/2 0 0]]\n",
      "[[0 sqrt(2)/2 0]\n",
      " [-sqrt(2)/2 0 0]\n",
      " [0 0 0]]\n",
      "[[sqrt(2)/2 0 0]\n",
      " [0 -sqrt(2)/2 0]\n",
      " [0 0 0]]\n",
      "[[0 sqrt(2)/2 0]\n",
      " [sqrt(2)/2 0 0]\n",
      " [0 0 0]]\n",
      "[[0 0 sqrt(2)/2]\n",
      " [0 0 0]\n",
      " [sqrt(2)/2 0 0]]\n",
      "[[0 0 0]\n",
      " [0 0 sqrt(2)/2]\n",
      " [0 sqrt(2)/2 0]]\n",
      "[[-sqrt(6)/6 0 0]\n",
      " [0 -sqrt(6)/6 0]\n",
      " [0 0 sqrt(6)/3]]\n"
     ]
    }
   ],
   "source": [
    "print(\"Second-order (full) harmonic basis\")\n",
    "for H_i in r2_harm:\n",
    "    print(H_i)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "a9823d7e-b0f7-496d-90f8-881d9a0e228d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Second-order symmetric harmonic basis\n",
      "[[sqrt(3)/3 0 0]\n",
      " [0 sqrt(3)/3 0]\n",
      " [0 0 sqrt(3)/3]]\n",
      "[[sqrt(2)/2 0 0]\n",
      " [0 -sqrt(2)/2 0]\n",
      " [0 0 0]]\n",
      "[[0 sqrt(2)/2 0]\n",
      " [sqrt(2)/2 0 0]\n",
      " [0 0 0]]\n",
      "[[0 0 sqrt(2)/2]\n",
      " [0 0 0]\n",
      " [sqrt(2)/2 0 0]]\n",
      "[[0 0 0]\n",
      " [0 0 sqrt(2)/2]\n",
      " [0 sqrt(2)/2 0]]\n",
      "[[-sqrt(6)/6 0 0]\n",
      " [0 -sqrt(6)/6 0]\n",
      " [0 0 sqrt(6)/3]]\n"
     ]
    }
   ],
   "source": [
    "print(\"Second-order symmetric harmonic basis\")\n",
    "for H_i in sym_harm:\n",
    "    print(H_i)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d16b3c3c-2cea-4d6e-8100-bd80050fc736",
   "metadata": {},
   "source": [
    "As a more complicated example, we calculate the inclusions for the fourth-order tensor space of left, right and mainsymmetric tensors. This we define as $sym(sym(\\mathcal{R}^3 \\otimes \\mathcal{R}^3) \\otimes sym(\\mathcal{R}^3 \\otimes \\mathcal{R}^3))$, allowing us to make use of the already calculated harmonic basis for second-order symmetric spaces.\n",
    "The special mixed-order Kronecker product `kron_mn` can be used to calculate inclusion tensors from the inclusion tensors of lower-order tensor spaces by decomposing the higher-order space into a dyadic product of lower-order spaces."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "ab799076-417f-43b9-8bed-98e588d4f5f1",
   "metadata": {},
   "outputs": [],
   "source": [
    "def kron_mn(m: int, n: int, t1: npt.NDArray, t2: npt.NDArray):\n",
    "    \"\"\"Special box product for inclusion tensor calculations.\"\"\"\n",
    "    o1 = len(t1.shape)\n",
    "    o2 = len(t2.shape)\n",
    "    ind = [0,\n",
    "           o1-m,\n",
    "           o1-m + o2-n,\n",
    "           o1 + o2-n,\n",
    "           o1 + o2]\n",
    "    s = []\n",
    "    indices = string.ascii_lowercase\n",
    "    for i in range(4):\n",
    "        s.append(indices[ind[i]:ind[i+1]])\n",
    "    s_full = indices[ind[0]:ind[-1]]\n",
    "    return oinsum(f\"{s[0]}{s[2]},{s[1]}{s[3]}->{s_full}\", t1, t2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "541907fc-5a16-4f77-bf37-123fe9ce63b2",
   "metadata": {},
   "source": [
    "The fourth-order lmr space contains multiple deviatoric subspaces for the orders 0 and 2, which are distinguished with the index `j`. The general formula involves a mixed-order kronecker product and a Clebsch-Gordan coefficient. One inclusion tensor also needs to be manually symmetrized to ensure main symmetry of the resulting tensor space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "040f2ef7-6e38-43dd-aae8-7c63ce494283",
   "metadata": {},
   "outputs": [],
   "source": [
    "def P4_lmr(k, j):\n",
    "    \"\"\"lmr inclusions P4k_j.\"\"\"\n",
    "    if k == 0:\n",
    "        if j == 1:\n",
    "            return kron_mn(0, 0, P2_sym(0), P2_sym(0))\n",
    "        elif j == 2:\n",
    "            return np.tensordot(kron_mn(2, 2, P2_sym(2), P2_sym(2)), cg(2, 2, 0), axes=4)\n",
    "        else:\n",
    "            raise ValueError(\"Invalid j.\")\n",
    "    elif k == 2:\n",
    "        if j == 1:\n",
    "            box = (kron_mn(0, 2, P2_sym(0), P2_sym(2))\n",
    "                   + kron_mn(2, 0, P2_sym(2), P2_sym(0)))/2\n",
    "            return np.tensordot(box, cg(1, 1, 2), axes=2)\n",
    "        elif j == 2:\n",
    "            return np.tensordot(kron_mn(2, 2, P2_sym(2), P2_sym(2)),\n",
    "                                cg(2, 2, 2), axes=4)\n",
    "        else:\n",
    "            raise ValueError(\"Invalid j.\")\n",
    "    elif k == 4:\n",
    "        return np.tensordot(kron_mn(2, 2, P2_sym(2), P2_sym(2)), cg(2, 2, 4), axes=4)\n",
    "    else:\n",
    "        raise ValueError(\"Invalid k.\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "455882eb-2614-4577-966a-bd86757b4c96",
   "metadata": {},
   "outputs": [],
   "source": [
    "lmr_harm = [np.tensordot(P4_lmr(k, j), D(k)[i], axes=k) for k, j in [(0,1), (0,2), (2,1), (2,2), (4,1)] for i in range(2*k+1)]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e4f0ceb6-23c7-452d-8b29-cf9e5ef2e8a8",
   "metadata": {},
   "source": [
    "Displaying fourth-order basis tensors in the $e_i$ basis is cumbersome. We transform to the second-order symmetric harmonic basis instead. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "4fcfc159-c557-4954-a70a-8249c68727fe",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]]\n",
      "[[0 0 0 0 0 0]\n",
      " [0 sqrt(5)/5 0 0 0 0]\n",
      " [0 0 sqrt(5)/5 0 0 0]\n",
      " [0 0 0 sqrt(5)/5 0 0]\n",
      " [0 0 0 0 sqrt(5)/5 0]\n",
      " [0 0 0 0 0 sqrt(5)/5]]\n",
      "[[0 1/2 0 0 0 0]\n",
      " [1/2 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]]\n",
      "[[0 0 1/2 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [1/2 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]]\n",
      "[[0 0 0 1/2 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [1/2 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]]\n",
      "[[0 0 0 0 1/2 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [1/2 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]]\n",
      "[[0 0 0 0 0 1/2]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [1/2 0 0 0 0 0]]\n",
      "[[0 0 0 0 0 0]\n",
      " [0 0 0 0 0 -sqrt(14)/7]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 sqrt(42)/14 0 0]\n",
      " [0 0 0 0 -sqrt(42)/14 0]\n",
      " [0 -sqrt(14)/7 0 0 0 0]]\n",
      "[[0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 -sqrt(14)/7]\n",
      " [0 0 0 0 sqrt(42)/14 0]\n",
      " [0 0 0 sqrt(42)/14 0 0]\n",
      " [0 0 -sqrt(14)/7 0 0 0]]\n",
      "[[0 0 0 0 0 0]\n",
      " [0 0 0 sqrt(42)/14 0 0]\n",
      " [0 0 0 0 sqrt(42)/14 0]\n",
      " [0 sqrt(42)/14 0 0 0 sqrt(14)/14]\n",
      " [0 0 sqrt(42)/14 0 0 0]\n",
      " [0 0 0 sqrt(14)/14 0 0]]\n",
      "[[0 0 0 0 0 0]\n",
      " [0 0 0 0 -sqrt(42)/14 0]\n",
      " [0 0 0 sqrt(42)/14 0 0]\n",
      " [0 0 sqrt(42)/14 0 0 0]\n",
      " [0 -sqrt(42)/14 0 0 0 sqrt(14)/14]\n",
      " [0 0 0 0 sqrt(14)/14 0]]\n",
      "[[0 0 0 0 0 0]\n",
      " [0 -sqrt(14)/7 0 0 0 0]\n",
      " [0 0 -sqrt(14)/7 0 0 0]\n",
      " [0 0 0 sqrt(14)/14 0 0]\n",
      " [0 0 0 0 sqrt(14)/14 0]\n",
      " [0 0 0 0 0 sqrt(14)/7]]\n",
      "[[0 0 0 0 0 0]\n",
      " [0 sqrt(2)/2 0 0 0 0]\n",
      " [0 0 -sqrt(2)/2 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]]\n",
      "[[0 0 0 0 0 0]\n",
      " [0 0 sqrt(2)/2 0 0 0]\n",
      " [0 sqrt(2)/2 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]]\n",
      "[[0 0 0 0 0 0]\n",
      " [0 0 0 1/2 0 0]\n",
      " [0 0 0 0 -1/2 0]\n",
      " [0 1/2 0 0 0 0]\n",
      " [0 0 -1/2 0 0 0]\n",
      " [0 0 0 0 0 0]]\n",
      "[[0 0 0 0 0 0]\n",
      " [0 0 0 0 1/2 0]\n",
      " [0 0 0 1/2 0 0]\n",
      " [0 0 1/2 0 0 0]\n",
      " [0 1/2 0 0 0 0]\n",
      " [0 0 0 0 0 0]]\n",
      "[[0 0 0 0 0 0]\n",
      " [0 0 0 0 0 sqrt(42)/14]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 sqrt(14)/7 0 0]\n",
      " [0 0 0 0 -sqrt(14)/7 0]\n",
      " [0 sqrt(42)/14 0 0 0 0]]\n",
      "[[0 0 0 0 0 0]\n",
      " [0 0 0 0 0 0]\n",
      " [0 0 0 0 0 sqrt(42)/14]\n",
      " [0 0 0 0 sqrt(14)/7 0]\n",
      " [0 0 0 sqrt(14)/7 0 0]\n",
      " [0 0 sqrt(42)/14 0 0 0]]\n",
      "[[0 0 0 0 0 0]\n",
      " [0 0 0 -sqrt(7)/14 0 0]\n",
      " [0 0 0 0 -sqrt(7)/14 0]\n",
      " [0 -sqrt(7)/14 0 0 0 sqrt(21)/7]\n",
      " [0 0 -sqrt(7)/14 0 0 0]\n",
      " [0 0 0 sqrt(21)/7 0 0]]\n",
      "[[0 0 0 0 0 0]\n",
      " [0 0 0 0 sqrt(7)/14 0]\n",
      " [0 0 0 -sqrt(7)/14 0 0]\n",
      " [0 0 -sqrt(7)/14 0 0 0]\n",
      " [0 sqrt(7)/14 0 0 0 sqrt(21)/7]\n",
      " [0 0 0 0 sqrt(21)/7 0]]\n",
      "[[0 0 0 0 0 0]\n",
      " [0 sqrt(70)/70 0 0 0 0]\n",
      " [0 0 sqrt(70)/70 0 0 0]\n",
      " [0 0 0 -2*sqrt(70)/35 0 0]\n",
      " [0 0 0 0 -2*sqrt(70)/35 0]\n",
      " [0 0 0 0 0 3*sqrt(70)/35]]\n"
     ]
    }
   ],
   "source": [
    "e_i_to_sym_transform = np.array(sym_harm)\n",
    "e_i_to_sym_transform_T = np.moveaxis(e_i_to_sym_transform, 0, -1)\n",
    "assert e_i_to_sym_transform_T.shape == (3,3,6)\n",
    "lmr_harm_sym = [np.tensordot(np.tensordot(e_i_to_sym_transform, H_i, axes=2), e_i_to_sym_transform_T, axes=2) for H_i in lmr_harm]\n",
    "for H_i in lmr_harm_sym:\n",
    "    print(H_i)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9b07c32f-d5f5-45d6-ae84-a9f7deb2ae2c",
   "metadata": {},
   "source": [
    "## Rotations in the harmonic basis\n",
    "\n",
    "Rotation matrices in the deviatoric basis are given by Wigner D-matrices. We first give small-d Wigner matrices, i.e. rotations around the y or x axis. The z axis rotation is particularly simple and will only be given in the deviatoric basis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "be6ff286",
   "metadata": {},
   "outputs": [],
   "source": [
    "def small_d(n, angle):\n",
    "    \"\"\"Classic Wigner-d matrix around the y-axis, formula copied from Wikipedia.\"\"\"\n",
    "    dim = 2*n+1\n",
    "    result = np.zeros((dim, dim), dtype=object)\n",
    "    fac = sympy.factorial\n",
    "    for ind_1, m in enumerate(range(-n, n+1)):\n",
    "        for ind_2, m_prime in enumerate(range(-n, n+1)):\n",
    "            factor = sympy.sqrt(fac(n+m_prime)\n",
    "                              * fac(n-m_prime)\n",
    "                              * fac(n+m)\n",
    "                              * fac(n-m))\n",
    "            s_min = max(0, m-m_prime)\n",
    "            s_max = min(n+m, n-m_prime)\n",
    "            summands = [((-1)**(m_prime-m+s)\n",
    "                         * sympy.cos(angle/2)**(2*n+m-m_prime-2*s)\n",
    "                         * sympy.sin(angle/2)**(m_prime-m+2*s)\n",
    "                         / (fac(n+m-s)*fac(s)*fac(m_prime-m+s)\n",
    "                            * fac(n-m_prime-s))) for s in range(s_min, s_max+1)]\n",
    "            result[ind_1, ind_2] = factor*np.sum(summands)\n",
    "    return result\n",
    "\n",
    "\n",
    "def small_dx(n, angle):\n",
    "    \"\"\"Wigner-d matrix around the x axis, formula copied from Wikipedia.\"\"\"\n",
    "    dim = 2*n+1\n",
    "    result = np.zeros((dim, dim), dtype=object)\n",
    "    fac = sympy.factorial\n",
    "    for ind_1, m in enumerate(range(-n, n+1)):\n",
    "        for ind_2, m_prime in enumerate(range(-n, n+1)):\n",
    "            factor = sympy.sqrt(fac(n+m_prime)\n",
    "                                * fac(n-m_prime)\n",
    "                                * fac(n+m)\n",
    "                                * fac(n-m))\n",
    "            s_min = max(0, m-m_prime)\n",
    "            s_max = min(n+m, n-m_prime)\n",
    "            summands = [((-1)**s*(1j)**(m-m_prime)\n",
    "                         * sympy.cos(angle/2)**(2*n+m-m_prime-2*s)\n",
    "                        * sympy.sin(angle/2)**(m_prime-m+2*s)\n",
    "                         / (fac(n+m-s)*fac(s)*fac(m_prime-m+s)\n",
    "                            * fac(n-m_prime-s))) for s in range(s_min, s_max+1)]\n",
    "            result[ind_1, ind_2] = factor*np.sum(summands)\n",
    "    return result"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cc788d5b-2019-4c12-acae-cc119ac9bcaf",
   "metadata": {},
   "source": [
    "These can be transformed into the deviatoric basis by using $T$. Matrices around multiple axes yield Euler angle parametrizations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "8ebb4622",
   "metadata": {},
   "outputs": [],
   "source": [
    "def small_d_D(n, angle):\n",
    "    \"\"\"Small d for y rotations in the D basis.\"\"\"\n",
    "    d_v = small_d(n, angle)\n",
    "    T_dv_n = T_dv(n)\n",
    "    return T_dv_n@d_v@adjungate(T_dv_n)\n",
    "\n",
    "\n",
    "def small_dx_D(n, angle):\n",
    "    \"\"\"Small d for x rotations in the D basis.\"\"\"\n",
    "    dx_v = small_dx(n, angle)\n",
    "    T_dv_n = T_dv(n)\n",
    "    return T_dv_n@dx_v@adjungate(T_dv_n)\n",
    "\n",
    "\n",
    "def small_dz_D(n, angle):\n",
    "    \"\"\" (simple) small d for z rotations in the D basis.\"\"\"\n",
    "    k = np.arange(-n, n+1)\n",
    "    dz_v = np.diag(np.exp(k*1j*angle))\n",
    "    T_dv_n = T_dv(n)\n",
    "    return T_dv_n@rot_z_v@adjungate(T_dv_n)\n",
    "    \n",
    "\n",
    "def rot_zyz_D(n, phi, theta, psi):\n",
    "    \"\"\"Rotation based on Euler angles.\"\"\"\n",
    "    return small_dz_D(n, phi)@small_d_D(n, theta)@small_dz_D(n, phi)\n",
    "\n",
    "\n",
    "def rot_zxz_D(n, phi, theta, psi):\n",
    "    \"\"\"Rotation based on Euler angles.\"\"\"\n",
    "    return small_dz_D(n, phi)@small_dx_D(n, theta)@small_dz_D(n, phi)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "07696255-36f2-4087-add0-3984c2b76836",
   "metadata": {},
   "source": [
    "Rotation matrices for harmonic bases are given by block matrix compositions of deviatoric rotation matrices. Note that deviatoric subspaces of the same order use the exact same deviatoric rotation matrices, allowing a particularly sparse description of rotations for very high-order tensor spaces. As an example, we construct a y-axis rotation for the harmonic basis of second-order symmetric tensors. As an example, we calculate a rotation matrix around the y axis for the symmetric harmonic basis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "59c15d9b-2afb-4f92-a6c0-7f17a8e427da",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1 0 0 0 0 0]\n",
      " [0 cos(alpha)**2/2 + 1/2 0 -sin(2*alpha)/2 0 sqrt(3)*(1 - cos(2*alpha))/4]\n",
      " [0 0 cos(alpha) 0 -sin(alpha) 0]\n",
      " [0 sin(2*alpha)/2 0 cos(alpha)**2/2 + 3*cos(2*alpha)/4 - 1/4 0 -sqrt(3)*sin(2*alpha)/2]\n",
      " [0 0 sin(alpha) 0 cos(alpha) 0]\n",
      " [0 sqrt(3)*(1 - cos(2*alpha))/4 0 sqrt(3)*sin(2*alpha)/2 0 cos(alpha)**2/2 + cos(2*alpha)/2]]\n"
     ]
    }
   ],
   "source": [
    "alpha = sympy.symbols(\"alpha\", real=True)\n",
    "rot = np.zeros((6,6), dtype=object)\n",
    "rot[0,0] = 1\n",
    "rot[1:,1:] = small_d_D(2, alpha)\n",
    "print(np_simplify(rot))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cbf20774",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0ea28984",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e931f6e5",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "60645c4d",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
