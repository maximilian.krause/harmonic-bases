# Harmonic Bases

This project hosts files related to the calculation of harmonic bases as outlined in the paper "Tensorial harmonic bases of arbitrary order with applications in elasticity, elastoviscoplasticity and texture-based modeling" (Krause and Böhlke, in review).
